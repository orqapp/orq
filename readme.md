# orq
orq is a requirement management application that (attempts to) support versioning, branching and hierarchies. 
It runs on Linux, macOS and Windows and is written in C++/Qt.

## [Latest Windows build](https://gitlab.com/orqapp/orq/-/jobs/artifacts/master/browse?job=orq)
Linux and macOS users are encouraged to build from source instead (Qt and CMake are needed).

## [Source code documentation](https://orqapp.gitlab.io/orq)
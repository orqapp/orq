#pragma once

#include "main.hpp"

#include <QHash>
#include <QIcon>

class Icons
{
private:
	Icons();
	static QHash<QString, QString> iconNames;
public:
	static QIcon get(const QString &name);
};
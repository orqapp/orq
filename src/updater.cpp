#include "updater.hpp"

Updater::Updater()
{
	network = new QNetworkAccessManager(this);
}

bool Updater::isLatest(bool dev)
{
	// We always want to get the latest tag
	auto reply = network->get(QNetworkRequest(
		QUrl("https://gitlab.com/api/v4/projects/17193193/repository/tags")));
	while (!reply->isFinished())
		QCoreApplication::processEvents();
	reply->deleteLater();
	QString tag(QJsonDocument::fromJson(reply->readAll()).array()[0].toObject()["name"].toString());
	// Also get commit if checking dev builds
	if (dev)
	{
		reply = network->get(QNetworkRequest(
			QUrl("https://gitlab.com/api/v4/projects/17193193/repository/commits")));
		while (!reply->isFinished())
			QCoreApplication::processEvents();
		reply->deleteLater();
		QString commit(QJsonDocument::fromJson(reply->readAll()).array()[0].toObject()["short_id"].toString());
		return QString("%1-%2").arg(tag).arg(commit.left(commit.length() - 1)) == ORQ_VERSION;
	}
	// Non-dev build only checks tag
	return tag == ORQ_VERSION;
}
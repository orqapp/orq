#pragma once

#include "icons.hpp"
#include "updater.hpp"
#include "datacontext.hpp"
#include "main.hpp"
#include "mainview.hpp"
#include "itemtoolselector.hpp"
#include "itemtypeselector.hpp"

#include <QMainWindow>
#include <QApplication>
#include <QStyle>
#include <QScreen>
#include <QDockWidget>
#include <QFileDialog>
#include <QStandardPaths>
#include <QMessageBox>
#include <QDesktopServices>
#include <QUrl>
#include <QFile>
#include <QToolButton>
#include <QSplitter>
#include <QGroupBox>
#include <QFontDatabase>
#include <QFont>

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	MainWindow(QWidget *parent = nullptr);
	~MainWindow();

	ItemToolSelector	*itemToolSelector;

private:
	QDockWidget	*dockValidation;
	MainView	*mainView;

	void addMenuBar();
	void createLayout();
};

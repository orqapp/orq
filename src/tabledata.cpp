#include "tabledata.hpp"

TableData::TableData()
{
}

QMap<QString, QStringList> TableData::get()
{
	return QMap<QString, QStringList>({
		{
			"Info", {
				"version integer default 1",
				"name text",
				"created integer default current_timestamp"
			}
		},
		{
			"ProjectVersions", {
				"uid integer",
				"name text",
				"created integer default current_timestamp",
				"width integer",
				"height integer"
			}
		},
		{
			"Items", {
				"uid integer",
				"label integer",
				"type integer",
				"description text",
				"color integer",
				"borderColor integer",
				"shape integer",
				"x integer",
				"y integer",
				"width integer default 128",
				"height integer default 64",
			}
		},
		{
			"Links", {
				"uid integer",
				"parent integer",
				"child integer",
				"description text"
			}
		},
		{
			"ItemVersions", {
				"projectVersion integer",
				"itemId integer",
				"itemVersion integer default 1",
				"type integer",
				"foreign key (projectVersion) references ProjectVersions(id)"
			}
		},
		{
			"ItemLabels", {
				"labelId integer",
				"itemId integer",
				"type integer",
				"foreign key (labelId) references ProjectVersions(id)"
			}
		},
		{
			"Media", {
				"parent integer not null",
				"format text default 'webp'",
				"data blob",
				"description text",
				"foreign key (parent) references Labels(id)"
			}
		},
		{
			"Labels", {
				"description text",
				"color integer"
			}
		},
		{
			"ValidationRules", {
				"description text",
				"enabled integer default 1",
				"data text"
			}
		}
	});
}
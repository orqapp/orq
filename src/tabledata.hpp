#pragma once

#include <QMap>
#include <QStringList>
#include <QVector>

class TableData
{
public:
	static QMap<QString, QStringList> get();
private:
	TableData();
};
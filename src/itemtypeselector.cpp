#include "itemtypeselector.hpp"

ItemTypeSelector::ItemTypeSelector(QWidget *parent) : QWidget(parent)
{
	auto layout = new QVBoxLayout();
	auto itemList = new QListWidget(this);
	itemList->setDragEnabled(true);
	itemList->addItems({
		"Problem",
		"Solution"
	});
	layout->addWidget(itemList);
	setLayout(layout);
	setMaximumWidth(200);
	setMinimumWidth(150);
}
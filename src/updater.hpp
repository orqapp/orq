#pragma once

#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QCoreApplication>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>

/**
 * Check for updates against GitLab
 */
class Updater : QObject
{
Q_OBJECT

public:
	/**
	 * Instance a new updater, also instances a new NetworkAccessManager
	 */
	Updater();

	/**
	 * Check if the current version (from ORQ_VERSION) is the latest version
	 * @param dev Check for dev/nightly builds
	 * @return Current version is latest version
	 */
	bool isLatest(bool dev);
private:
	QNetworkAccessManager	*network;
};
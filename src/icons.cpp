#include "icons.hpp"

// Static definition
QHash<QString, QString> Icons::iconNames = {
	// File menu
	{ "file-new",				"document-new"				},
	{ "file-open",				"document-open"				},
	{ "file-save-as",			"document-save-as"			},
	{ "file-close",				"document-close"				},
	{ "file-quit",				"application-exit"			},
	// Edit menu
	{ "edit-rename",				"text-field"					},
	{ "edit-reload",				"view-refresh"				},
	// About menu
	{ "about-app",				"help-about"					},
	{ "about-qt",				"qt"							},
	{ "about-licenses",			"license"					},
	{ "about-update",			"download"					},
	// Tools
	{ "tools-move",				"handle-move"				},
	{ "tools-link",				"draw-line"					},
	// Other menus
	{ "menu-edit",				"document-edit"				},
	{ "menu-delete",				"delete"						},
	// Text formatting
	{ "format-bold",				"format-text-bold"			},
	{ "format-italic",			"format-text-italic"			},
	{ "format-underline",		"format-text-underline"		},
	{ "format-strikethrough",	"format-text-strikethrough"	},
	// Validation engine
	{ "validate-ok",				"emblem-checked"				},
	{ "validate-fail",			"emblem-error"				},
	{ "validate-disabled",		"emblem-pause"				},
	{ "validate-none",			"emblem-question"			},
};

Icons::Icons() = default;

QIcon Icons::get(const QString &name)
{
#ifdef Q_OS_LINUX
	// Use system icons on Linux
	if (useCustomStyle())
		return QIcon(QString(":/res/ic/%1/%2.svg")
			.arg(useDarkVariant() ? "dark" : "light").arg(iconNames.value(name)));
	else
		return QIcon::fromTheme(iconNames.value(name));
#else
	// Always use bundled icons elsewhere
	return QIcon(QString(":/res/ic/%1/%2.svg")
		.arg(useDarkVariant() ? "dark" : "light").arg(iconNames.value(name)));
#endif
}
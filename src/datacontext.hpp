#pragma once

#include "tabledata.hpp"

#include <QDebug>
#include <QSqlDatabase>
#include <QFile>
#include <QFileInfo>
#include <QMapIterator>
#include <QSqlQuery>

class DataContext
{
public:
	DataContext(const QString &path);

	~DataContext();

	static void close();
	QString projectName();
private:
	QSqlDatabase	database;

	bool create(const QString &projectName);
};



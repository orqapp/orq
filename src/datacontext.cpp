#include "datacontext.hpp"

DataContext::DataContext(const QString &path)
{
	// Check if SQLite is available
	if (!QSqlDatabase::isDriverAvailable("QSQLITE"))
	{
		qCritical() << "error: sqlite is not available";
		return;
	}

	// Instance database
	database = QSqlDatabase::addDatabase("QSQLITE");
	// Check if file already exists
	auto fileExists = QFile(path).exists();
	// Open file
	database.setDatabaseName(path);
	database.open();

	// Create if it was just created
	if (!fileExists)
	{
		// Get file name
		auto fileName = QFileInfo(path).fileName();
		if (fileName.contains("."))
			fileName = fileName.left(fileName.lastIndexOf("."));
		// Call create method
		if (!create(fileName))
			qCritical() << "error: failed to create initial database";
	}
}

DataContext::~DataContext()
{
	database.close();
}

bool DataContext::create(const QString &projectName)
{
	// Prepared query
	QSqlQuery query(database);

	// Loop through table data
	QMapIterator<QString, QStringList> iterator(TableData::get());
	while (iterator.hasNext())
	{
		iterator.next();
		// Create new table for entry
		if (!query.exec(QString("create table %1 (%2)")
			.arg(iterator.key())
			.arg(iterator.value().join(", "))))
		{
			qCritical() << "error: failed to create" << iterator.key() << "table";
			return false;
		}
	}

	// Insert info table
	query.prepare("insert into Info (name) values (:name)");
	query.bindValue(":name", projectName);
	if (!query.exec())
	{
		qCritical() << "error: failed to insert into Info table";
		return false;
	}

	return true;
}

void DataContext::close()
{
	// Remove all database connections
	for (auto &connection : QSqlDatabase::connectionNames())
		QSqlDatabase::removeDatabase(connection);
}

QString DataContext::projectName()
{
	QSqlQuery query("select name from Info", database);
	query.next();
	return query.value(0).toString();
}

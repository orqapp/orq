#include "item.hpp"

Item::Item(qint64 id) : id(id)
{
}

int Item::rowId() const
{
	return id;
}

void Item::setPos(int x, int y)
{
	// TODO
}

QString Item::toString() const
{
	return QString("Item %1").arg(id);
}
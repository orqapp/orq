#pragma once

class MainView;

#include "item.hpp"
#include "mainwindow.hpp"

#include <QGraphicsView>
#include <QGraphicsScene>
#include <QColor>
#include <QMap>
#include <QFont>
#include <QGraphicsTextItem>
#include <QDragMoveEvent>
#include <QDebug>
#include <QTextDocument>
#include <QTextCursor>
#include <QTextDocumentFragment>
#include <QMenu>

/**
 * Main view showing all items and links
 */
class MainView : public QGraphicsView
{
	Q_OBJECT

public:
	/**
	 * Create a new main view
	 * @param parent Parent, probably MainWindow
	 */
	explicit MainView(QWidget *parent = nullptr);

	/**
	 * Set if a project is currently loaded
	 * (disables adding items and shows "no project loaded" text)
	 * @param loaded A project is currently loaded
	 */
	void setProjectLoaded(bool loaded);

private:
	/// Parent main window
	QWidget				*parent;
	/// Main graphics scene for adding items
	QGraphicsScene		*scene;
	/// Window background color used for background color in items
	QColor				backgroundColor;
	/// Text shown when no project is loaded
	QGraphicsTextItem	*noProjectText;
	/// If we're moving anything
	QGraphicsItemGroup *movingItem = nullptr;
	/// Start position for link
	QGraphicsItemGroup *linkStart = nullptr;
	/// Line shown when creating new links
	QGraphicsLineItem *tempLink = nullptr;
	/// Item height, width is x2
	const int itemSize = 64;
	QPointF snapToGrid(const QPoint &pos);
	QGraphicsItemGroup *newGraphicsItem(const QString &text, int x, int y, int width, int height, const Item &item);
	void updateLinkPos(QGraphicsItemGroup *item, QPointF pos);
	Item getGroupItem(QGraphicsItemGroup *group);

protected:
	void dragMoveEvent(QDragMoveEvent *event) override;
	void resizeEvent(QResizeEvent *event) override;
	void dropEvent(QDropEvent *event) override;
	void mousePressEvent(QMouseEvent *event) override;
	void mouseMoveEvent(QMouseEvent *event) override;
	void mouseReleaseEvent(QMouseEvent *event) override;
};
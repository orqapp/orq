#pragma once

#include "item.hpp"

#include <QGraphicsLineItem>
#include <QGraphicsPolygonItem>

class Link
{
public:
	Link(Item &child, Item &parent);

	Item					&child;
	Item					&parent;
	QGraphicsLineItem		*line;
	QGraphicsPolygonItem	*arrow;
};
#include "mainview.hpp"

MainView::MainView(QWidget *parent) : parent(parent), QGraphicsView(parent)
{
	// Create scene
	scene = new QGraphicsScene(parent);
	setScene(scene);
	// Set default background color
	if (parent != nullptr)
		backgroundColor = parent->palette().color(parent->backgroundRole());

	// TODO: Project loading
	QFont font;
	font.setPointSize(18);
	noProjectText = scene->addText("No Project Loaded", font);
	setEnabled(false);
	setAlignment(Qt::AlignTop | Qt::AlignLeft);
	// Drag-and-drop
	setAcceptDrops(true);
}

void MainView::dragMoveEvent(QDragMoveEvent *event)
{
	if (event->source() != nullptr && event->source()->isWidgetType())
		event->acceptProposedAction();
}

void MainView::resizeEvent(QResizeEvent *event)
{
	// Update 'no project loaded' text when resizing window
	auto rect = noProjectText->boundingRect();
	noProjectText->setX((event->size().width()  / 2.0) - (rect.width()  / 2.0));
	noProjectText->setY((event->size().height() / 2.0) - (rect.height() / 2.0));

	// Make sure scene is at least as large as the view
	if (scene->sceneRect().width() < event->size().width() && scene->sceneRect().height() < event->size().height())
		scene->setSceneRect(0, 0, event->size().width(), event->size().height());
	else
		scene->setSceneRect(QRectF());
}

void MainView::setProjectLoaded(bool loaded)
{
	setEnabled(loaded);
	noProjectText->setVisible(!loaded);
}

void MainView::dropEvent(QDropEvent *event)
{
	auto pos = mapToScene(event->pos());
	// Add item to database
	// TODO
	// Snap to grid
	auto gridPos = snapToGrid(pos.toPoint());
	// Set size and position in db
	// TODO
	// Add item to view
	scene->addItem(newGraphicsItem("item", gridPos.x(), gridPos.y(), itemSize * 2, itemSize, Item(0)));
	// TODO: Open item edit window
}

QPointF MainView::snapToGrid(const QPoint &pos)
{
	// 2^5=32
	const unsigned int gridSize = 5;
	auto scenePos = mapToScene(pos).toPoint();
	return mapToScene(
		static_cast<int>(((static_cast<unsigned int>(scenePos.x())) >> gridSize << gridSize) - 64u),
		static_cast<int>(((static_cast<unsigned int>(scenePos.y())) >> gridSize << gridSize) - 32u));
}

QGraphicsItemGroup *MainView::newGraphicsItem(const QString &text, int x, int y, int width,
	int height, const Item &item)
{
	auto group = new QGraphicsItemGroup();
	auto textItem = new QGraphicsTextItem(group);
	// Check if plain text is too long
	QTextDocument doc;
	doc.setMarkdown(text);
	const int maxLength = 46;
	if (doc.toPlainText().length() > maxLength)
	{
		QTextCursor cursor(&doc);
		// Move to end
		cursor.movePosition(QTextCursor::End);
		// Keep moving while too long
		while (doc.toPlainText().length() > maxLength)
			cursor.deletePreviousChar();
		// Remove last word to avoid cropping
		cursor.select(QTextCursor::WordUnderCursor);
		if (cursor.selection().toPlainText().length() != doc.toPlainText().length())
			cursor.insertText("");
		cursor.clearSelection();
		// Remove all trailing spaces
		cursor.movePosition(QTextCursor::End);
		while (doc.toPlainText().endsWith(" ") && doc.toPlainText().length() > 0)
			cursor.deletePreviousChar();
		// Insert ... at the end
		cursor.insertText("...");
	}
	// If no description, set text to item string
	if (doc.toPlainText().length() <= 0)
		doc.setHtml(QString("<small>%1</small>").arg(item.toString()));
	textItem->setHtml(doc.toHtml());
	textItem->setZValue(15);
	textItem->setTextWidth(width);
	// Border shape
	auto shapeItem = new QGraphicsRectItem(0, 0, width, height, group);
	shapeItem->setBrush(QBrush(backgroundColor));
	// All same default border color for now
	QColor color(10233776);
	color.setAlpha(200);
	shapeItem->setPen(QPen(color));
	// Add it to the main group
	group->addToGroup(textItem);
	group->addToGroup(shapeItem);
	group->setPos(x, y);
	group->setData(0x100, item.rowId());
	group->setZValue(10);
	return group;
}

void MainView::mousePressEvent(QMouseEvent *event)
{
	if (event->button() != Qt::LeftButton)
		return;
	auto item = itemAt(event->pos());
	// An item was found
	if (item != nullptr && item->group() != nullptr)
	{
		if (((MainWindow*) parent)->itemToolSelector->linkBtn->isChecked())
		{
			// We're trying to create a link
			linkStart = item->group();
			// Create temp link
			auto scenePos = mapToScene(event->pos());
			tempLink = new QGraphicsLineItem(QLineF(scenePos, scenePos));
			tempLink->setPen(QPen(QColor(0, 255, 0, 128)));
			scene->addItem(tempLink);
		}
		else
		{
			// We're moving an item
			movingItem = item->group();
			movingItem->setOpacity(0.6);
		}
	}
}

void MainView::mouseMoveEvent(QMouseEvent *event)
{
	if (movingItem != nullptr)
	{
		// Update item position
		movingItem->setPos(mapToScene(snapToGrid(event->pos()).toPoint()));
		// Update link
		updateLinkPos(movingItem, movingItem->pos());
	}
	// Update temp link
	if (tempLink != nullptr)
	{
		auto tempLine = tempLink->line();
		tempLine.setP2(mapToScene(event->pos()));
		tempLink->setLine(tempLine);
	}
	// Show hand when trying to move item
	if (((MainWindow*) parent)->itemToolSelector->moveBtn->isChecked()
		&& itemAt(event->pos()) != nullptr
		&& itemAt(event->pos())->group() != nullptr
		&& itemAt(event->pos())->group()->type() != 0x100)
		setCursor(movingItem == nullptr ? Qt::OpenHandCursor : Qt::ClosedHandCursor);
	else
		unsetCursor();
}

void MainView::mouseReleaseEvent(QMouseEvent *event)
{
	if (event->button() == Qt::RightButton && itemAt(event->pos()) != nullptr
		&& itemAt(event->pos())->group() != nullptr)
	{
		auto pos = event->pos();
		auto menu = new QMenu(this);
		// Clicking on link
		auto item = itemAt(pos);
		auto group = item->group();
		// If type of 0x100, it's probably a link
		if (group->type() == 0x100)
		{
			// Hopefully clicked a link
			auto actionDelete = menu->addAction(Icons::get("menu-delete"), "Delete");
			QAction::connect(actionDelete, &QAction::triggered, [](bool checked) {
				//Item childItem(item->data(0x100).toLongLong());
				// Remove in front end
				// TODO
			});
			menu->popup(mapToGlobal(pos));
			return;
		}
		// Edit
		menu->addAction(Icons::get("menu-edit"), "Edit");
		// Delete
		menu->addAction(Icons::get("menu-delete"), "Delete");
		// Show menu
		menu->popup(mapToGlobal(event->pos()));
		return;
	}

	// Button released while moving item
	if (movingItem != nullptr)
	{
		auto pos = movingItem->pos();
		// Update link if needed
		updateLinkPos(movingItem, pos);
		// Update pos in db
		getGroupItem(movingItem).setPos(pos.x(), pos.y());
		// Reset opacity and moving
		movingItem->setOpacity(1.0);
		movingItem = nullptr;
	}

	// Reset temp link on release
	if (tempLink != nullptr)
	{
		scene->removeItem(tempLink);
		tempLink = nullptr;
	}

	// Released while creating link
	if (linkStart != nullptr)
	{
		auto linkStartItem = getGroupItem(linkStart);
		auto group = itemAt(event->pos()) == nullptr ? nullptr : itemAt(event->pos())->group();
		auto groupItem = getGroupItem(group);
		// Try to link to nothing or self
		if (group == nullptr || linkStartItem.rowId() == groupItem.rowId())
		{
			linkStart = nullptr;
			return;
		}
		auto toPos = group->pos();
		if (toPos.x() == 0 && toPos.y() == 0)
			return;
		// Create and add link
		// TODO
	}
}

void MainView::updateLinkPos(QGraphicsItemGroup *item, QPointF pos)
{
	// TODO
}

Item MainView::getGroupItem(QGraphicsItemGroup *group)
{
	// TODO
	return Item(0);
}


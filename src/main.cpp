#include "mainwindow.hpp"
#include "main.hpp"

#include <QApplication>
#include <QCoreApplication>
#include <QCommandLineParser>

bool customStyle;
bool darkVariant;

int main(int argc, char *argv[])
{
	// Setup application variables
	QCoreApplication::setOrganizationName("orqapp");
	QCoreApplication::setApplicationName("orq");
	QCoreApplication::setApplicationVersion(ORQ_VERSION);

	// Create Qt application
	QApplication app(argc, argv);

	// Command line options
	QCommandLineParser parser;
	parser.addOptions({
		{"custom-style", QCoreApplication::translate("main",
			"Force custom style, default on non-Linux")},
		{"system-style", QCoreApplication::translate("main",
			"Force system style, default on Linux")},
		{"dark-variant", QCoreApplication::translate("main",
			"Use dark variant of custom style")}
	});
	parser.addVersionOption();
	parser.addHelpOption();
	parser.process(app);

	if (parser.isSet("custom-style"))
		customStyle = true;
	else if (parser.isSet("system-style"))
		customStyle = false;
	else
	{
#ifdef Q_OS_LINUX
		customStyle = false;
#else
		customStyle = true;
#endif
	}
	darkVariant = parser.isSet("dark-variant");

	// Create main window
	MainWindow window;

	// Show main window and run main Qt loop
	window.show();
	return app.exec();
}

bool useCustomStyle()
{
	return customStyle;
}

bool useDarkVariant()
{
	return darkVariant;
}
#pragma once

#include "datacontext.hpp"

#include <QString>
#include <QFile>

class Project
{
public:
	explicit Project(QString path);
	static Project *fromCompressed(const QString &path, QString *error);
	DataContext data();
	QString name();
	QByteArray compress();
	void copyTo(const QString &toPath);

private:
	bool open;
	QString path;
};
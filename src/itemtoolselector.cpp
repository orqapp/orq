#include "itemtoolselector.hpp"

ItemToolSelector::ItemToolSelector(QWidget *parent) : QHBoxLayout(parent)
{
	// Move item
	moveBtn = new QToolButton(parent);
	moveBtn->setIcon(Icons::get("tools-move"));
	moveBtn->setCheckable(true);
	moveBtn->setChecked(true);
	moveBtn->setToolTip("Move an already created item");
	addWidget(moveBtn);
	// Create link
	linkBtn = new QToolButton(parent);
	linkBtn->setIcon(Icons::get("tools-link"));
	linkBtn->setCheckable(true);
	linkBtn->setToolTip("Create a new link between already created items");
	addWidget(linkBtn);
	// Create spacer to left align
	addStretch(1);
	// Toggle
	QAbstractButton::connect(moveBtn, &QAbstractButton::clicked, [this](bool checked) {
		moveBtn->setChecked(true);
		linkBtn->setChecked(false);
	});
	QAbstractButton::connect(linkBtn, &QAbstractButton::clicked, [this](bool checked) {
		linkBtn->setChecked(true);
		moveBtn->setChecked(false);
	});
}
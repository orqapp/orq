#pragma once

#include <QByteArray>
#include <QPair>
#include <QtGlobal>
#include <QString>

/**
 * Data for any type of item, not actual item shown graphically
 */
class Item
{
private:
	/**
	 * Row id in database
	 */
	qint64 id;
public:
	/**
	 * @brief Create a new item from row id
	 * @param id Row id 
	 */
	explicit Item(qint64 id);

	/**
	 * All different types of items
	 */
	enum ItemType
	{
		Requirement 				= 0,
		SolutionArgument			= 1,
		DomainKnowledge				= 2,
		ArchitecturalDescription	= 3,
		TestCase 					= 4
	};

	/**
	 * Row id in database
	 */
	int rowId() const;

	/**
	 * Get globally unique id
	 */
	int guid();

	/**
	 * Set globally unique id
	 */
	void setGuid(int guid);

	/**
	 * Version of the item
	 */
	int version();

	/**
	 * Hash changed when modifying item properties
	 */
	QByteArray hash();

	/**
	 * Get if item (and children) should be shown
	 */
	bool shown();

	/**
	 * Set if item (and children) should be shown
	 */
	void setShown();

	/**
	 * Get position of item in view
	 * @return X, Y
	 */
	QPair<int, int> pos();

	/**
	 * Set position of item in view
	 */
	void setPos(int x, int y);

	/**
	 * Get size of item in view
	 * @return Width, height
	 */
	QPair<int, int> size();

	/**
	 * Set size of item in view
	 */
	void setSize(int w, int h);

	/**
	 * Get type of item
	 */
	ItemType type();

	/**
	 * Set type of item
	 */
	void setType(ItemType type);

	/**
	 * Add a new child
	 */
	void addChild(Item child);

	/**
	 * Remove specific child, does nothing if not a child
	 * @param child
	 */
	void removeChild(Item child);

	/**
	 * Get all current children
	 */
	QVector<Item> children();

	/**
	 * Add a new parent
	 * (usually when adding a child, the parent gets set too)
	 */
	void addParent(Item parent);

	/**
	 * Remove a specific parent, does nothing if not a parent
	 * @param parent
	 */
	void removeParent(Item parent);

	/**
	 * Get all current parents
	 * @return
	 */
	QVector<Item> parents();

	/**
	 * Check if a specific value is not set
	 */
	bool isPropertyNull(QString columnName);

	/**
	 * Represent item as user-friendly string,
	 * can be shown in user interface
	 */
	QString toString() const;
};
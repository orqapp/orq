#pragma once

#include "icons.hpp"

#include <QVBoxLayout>
#include <QToolButton>

class ItemToolSelector : public QHBoxLayout
{
	Q_OBJECT

public:
	explicit ItemToolSelector(QWidget *parent = nullptr);

	QToolButton	*linkBtn;
	QToolButton	*moveBtn;
};
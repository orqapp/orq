#include "mainwindow.hpp"
#include <QMenuBar>
#include <QMenu>
#include <QAction>
#include <QtGlobal>
#include <QtDebug>

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent)
{
	// Set minimum and initial size
	setMinimumSize(960, 540);
	resize(1280, 720);

	// Center window on screen
	setGeometry(QStyle::alignedRect(
		Qt::LeftToRight,
		Qt::AlignCenter,
		size(),
		QGuiApplication::screens().at(0)->availableGeometry()
	));

	// Set window title and icon
	setWindowTitle("orq");
	setWindowIcon(QIcon(":/res/logo/orq.svg"));

	// Set style if set
	if (useCustomStyle())
	{
		// Load and set style
		QFile styleFile(QString(":/res/style/%1.qss").arg(useDarkVariant() ? "dark" : "light"));
		styleFile.open(QFile::ReadOnly);
		QApplication::setStyle("fusion");
		setStyleSheet(styleFile.readAll());
		// Load and set font
		QFontDatabase::addApplicationFont(":/res/font/NotoSans-Regular.ttf");
		QApplication::setFont(QFont("Noto Sans Regular", QApplication::font().pointSize()));
	}

	// Setup the rest of the stuff
	addMenuBar();
	createLayout();
}

MainWindow::~MainWindow()
{
}

void MainWindow::addMenuBar()
{
	// File menu
	auto fileMenu = menuBar()->addMenu("File");
	// New
	auto fileNew = fileMenu->addAction(Icons::get("file-new"), "New...");
	fileNew->setShortcut(QKeySequence::New);
	QAction::connect(fileNew, &QAction::triggered, [this](bool checked) {
		// Temp
		mainView->setProjectLoaded(true);
		return;
		auto fileName = QFileDialog::getSaveFileName(
			this,
			"New Project",
			QStandardPaths::locate(QStandardPaths::DocumentsLocation, ""),
			"orq project(*.orq)"
		);
		if (fileName.length() > 0)
		{
			// TODO: newProject(), reloadProject()
			// Testing project creation
			DataContext dataContext(fileName);
		}
	});
	// Open
	auto fileOpen = fileMenu->addAction(Icons::get("file-open"), "Open...");
	fileOpen->setShortcut(QKeySequence::Open);
	// Save as
	auto fileSaveAs = fileMenu->addAction(Icons::get("file-save-as"), "Save As...");
	fileSaveAs->setShortcut(QKeySequence::SaveAs);
	// Close project
	fileMenu->addSeparator();
	auto fileCloseProject = fileMenu->addAction(Icons::get("file-close"), "Close Project");
	fileCloseProject->setShortcut(QKeySequence::Close);
	QAction::connect(fileCloseProject, &QAction::triggered, [this](bool checked) {
		mainView->setProjectLoaded(false);
	});
	// Quit
	auto fileQuit = fileMenu->addAction(Icons::get("file-quit"), "Quit");
	fileQuit->setShortcut(QKeySequence::Quit);
	QAction::connect(fileQuit, &QAction::triggered, [this](bool checked) {
		close();
	});

	// Edit menu
	auto editMenu = menuBar()->addMenu("Edit");
	// Rename project
	editMenu->addAction(Icons::get("edit-rename"), "Rename Project...");
	// Reload project
	editMenu->addAction(Icons::get("edit-reload"), "Reload Project");

	// View menu
	auto viewMenu = menuBar()->addMenu("View");
	// Validation engine
	auto viewValidate = viewMenu->addAction("Validation Engine");
	viewValidate->setCheckable(true);

	// About menu
	auto aboutMenu = menuBar()->addMenu("About");
	// About app
	auto aboutVersion = aboutMenu->addAction(
		Icons::get("about-app"),
		QString("orq %1").arg(ORQ_VERSION));
	aboutVersion->setEnabled(false);
	// About Qt
	auto aboutQt = aboutMenu->addAction(Icons::get("about-qt"), "About Qt");
	QAction::connect(aboutQt, &QAction::triggered, [this](bool checked) {
		QMessageBox::aboutQt(this);
	});
	// Third-party licenses
	aboutMenu->addSeparator();
	auto aboutLicenses = aboutMenu->addAction(Icons::get("about-licenses"), "Licenses");
	QAction::connect(aboutLicenses, &QAction::triggered, [this](bool checked) {
		QDesktopServices::openUrl(QUrl("https://gitlab.com/orqapp/orq/blob/master/third_party.md"));
	});
	// Check for updates
	auto aboutUpdate = aboutMenu->addAction(Icons::get("about-update"), "Check for Updates");
	QAction::connect(aboutUpdate, &QAction::triggered, [this](bool checked) {
		// For now, we always check for the latest dev build
		if (Updater().isLatest(true))
			QMessageBox::information(this, "Updater", "You are running the latest version");
		else
			QDesktopServices::openUrl(
				QUrl("https://gitlab.com/orqapp/orq/-/jobs/artifacts/master/browse?job=orq"));
	});
}

void MainWindow::createLayout()
{
	// Main splitter
	auto splitter = new QSplitter();
	setCentralWidget(splitter);

	// Sidebar
	auto sidebar = new QVBoxLayout();
	// Tools
	auto tools = new QGroupBox("Tools");
	itemToolSelector = new ItemToolSelector(tools);
	tools->setLayout(itemToolSelector);
	sidebar->addWidget(tools);
	// Create item
	auto types = new QGroupBox("Type");
	auto typesLayout = new QVBoxLayout();
	typesLayout->addWidget(new ItemTypeSelector(splitter));
	types->setLayout(typesLayout);
	sidebar->addWidget(types);
	// Add sidebar to splitter
	auto sidebarWidget = new QWidget();
	sidebarWidget->setLayout(sidebar);
	sidebarWidget->setMinimumWidth(150);
	sidebarWidget->setMaximumWidth(250);
	splitter->addWidget(sidebarWidget);

	// Main view
	mainView = new MainView(this);
	splitter->addWidget(mainView);

	// Create validation engine
	// TODO
}
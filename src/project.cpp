#include "project.hpp"

#include <utility>

Project::Project(QString path) : open(true), path(std::move(path))
{
	// Append file extension if needed
	if (!path.endsWith(".orq"))
		this->path = path.append(".orq");
	// Create initial database if needed
	DataContext(this->path);
}

Project *Project::fromCompressed(const QString &path, QString *error)
{
	// Name of the output file (.orqz -> .orq)
	auto newPath = path.left(path.length() - 1);
	// Check if decompressed file already exists
	QFile newFile(newPath);
	if (newFile.exists())
	{
		*error = QString("file with name \"%1\" already exists").arg(newPath);
		return nullptr;
	}
	// Load compressed file
	QFile compressed(path);
	compressed.open(QIODevice::ReadOnly);
	// Decompress it
	QByteArray decompressed = qUncompress(compressed.readAll());
	// Create new decompressed copy
	newFile.open(QIODevice::WriteOnly);
	newFile.write(decompressed);
	// Remove old project
	QFile::remove(path);
	// Load newly created project
	return new Project(newPath);
}

DataContext Project::data()
{
	return DataContext(path);
}

QString Project::name()
{
	return data().projectName();
}

QByteArray Project::compress()
{
	QFile file(path);
	file.open(QIODevice::ReadOnly);
	return qCompress(file.readAll());
}

void Project::copyTo(const QString &toPath)
{
	// Original project file
	QFile file(path);
	file.open(QIODevice::ReadOnly);
	// New copy to create
	QFile copy(toPath);
	file.open(QIODevice::WriteOnly);
	// Write compressed if set
	copy.write(toPath.endsWith(".orqz") ? qCompress(file.readAll()) : file.readAll());
}